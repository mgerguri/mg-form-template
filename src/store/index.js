import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        input_options: [
            {
                name: 'First Name',
                value: null,
                show: true,
                full_width: false,
                rules: [
                    {
                        rule_name: 'required',
                        rule_label: 'Required',
                        type: 'BOOLEAN',
                        validate: true
                    },
                    {
                        rule_name: 'alpha_spaces',
                        rule_label: 'Alpha Spaces',
                        type: 'BOOLEAN',
                        validate: true
                    }
                ]
            },
            {
                name: 'Last Name',
                value: null,
                show: true,
                full_width: false,
                rules: [
                    {
                        rule_name: 'required',
                        rule_label: 'Required',
                        type: 'BOOLEAN',
                        validate: true
                    },
                    {
                        rule_name: 'alpha_spaces',
                        rule_label: 'Alpha Spaces',
                        type: 'BOOLEAN',
                        validate: true
                    }
                ]
            },
            {
                name: 'Address',
                value: null,
                show: true,
                full_width: true,
                rules: [
                    {
                        rule_name: 'required',
                        rule_label: 'Required',
                        type: 'BOOLEAN',
                        validate: true
                    },
                    {
                        rule_name: 'max',
                        rule_label: 'Max Length',
                        type: 'VALUE',
                        validate: 10
                    }
                ]
            },
            {
                name: 'Zip Code',
                value: null,
                show: true,
                full_width: false,
                rules: [
                    {
                        rule_name: 'required',
                        rule_label: 'Required',
                        type: 'BOOLEAN',
                        validate: true
                    },
                    {
                        rule_name: 'numeric',
                        rule_label: 'Numbers Only',
                        type: 'BOOLEAN',
                        validate: true
                    }
                ]
            },
            {
                name: 'City',
                value: null,
                show: true,
                full_width: false,
                rules: [
                    {
                        rule_name: 'required',
                        rule_label: 'Required',
                        type: 'BOOLEAN',
                        validate: true
                    },
                    {
                        rule_name: 'alpha_spaces',
                        rule_label: 'Alpha Spaces',
                        type: 'BOOLEAN',
                        validate: true
                    }
                ]
            },
            {
                name: 'State',
                value: null,
                show: true,
                full_width: false,
                rules: [
                    {
                        rule_name: 'required',
                        rule_label: 'Required',
                        type: 'BOOLEAN',
                        validate: true
                    },
                    {
                        rule_name: 'alpha_spaces',
                        rule_label: 'Alpha Spaces',
                        type: 'BOOLEAN',
                        validate: true
                    }
                ]
            },
            {
                name: 'Country',
                value: null,
                show: true,
                full_width: false,
                rules: [
                    {
                        rule_name: 'required',
                        rule_label: 'Required',
                        type: 'BOOLEAN',
                        validate: true
                    },
                    {
                        rule_name: 'alpha_spaces',
                        rule_label: 'Alpha Spaces',
                        type: 'BOOLEAN',
                        validate: true
                    }
                ]
            },
            {
                name: 'Email',
                value: null,
                show: true,
                full_width: true,
                rules: [
                    {
                        rule_name: 'required',
                        rule_label: 'Required',
                        type: 'BOOLEAN',
                        validate: true
                    },
                    {
                        rule_name: 'email',
                        rule_label: 'Email',
                        type: 'BOOLEAN',
                        validate: true
                    }
                ]
            },
        ]
    },
    mutations: {
        setInputOptions(state, payload) {
            state.input_options = payload
        }
    },
    actions: {},
    modules: {}
})
